Rails.application.routes.draw do
	# The priority is based upon order of creation: first created -> highest priority.
	# See how all your routes lay out with "rake routes".
	root 'pages#index'

	get '/posts', to: 'pages#posts', as: 'posts'
	get '/post/:id', to: 'pages#post', as: 'post'

	get '/login', to: 'sessions#new'
	post '/login', to: 'sessions#create'
	delete '/logout', to: 'sessions#destroy'

	get '/admin', to: 'admin#index'
	namespace :admin do
		resources :posts
		resources :categories
		resources :users
		resources :privileges
		resources :comments
	end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable
end
