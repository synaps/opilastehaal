json.array!(@users) do |user|
  json.extract! user, :id, :Nickname, :Name, :Surname, :Group, :password, :password_confirmation
  json.url user_url(admin_user, format: :json)
end
