json.array!(@comments) do |comment|
  json.extract! comment, :id, :Text, :Date, :Allowed, :author_id, :post_id
  json.url admin_comment_url(comment, format: :json)
end
