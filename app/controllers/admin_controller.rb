class AdminController < ApplicationController
  layout 'admin_application'
  #before_action 'allowed?'

  def index
  end

  private
  def allowed?
    unless current_user.admin? or current_user.root?
      redirect_to root_path
    end
  end

  def check_permission(permission)
    unless current_user.check_privilege permission
      redirect_to admin_path
    end
  end
end
