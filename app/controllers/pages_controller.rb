class PagesController < ApplicationController
	def index
		@posts = Admin::Post.where({Published: true}).last(5).reverse
	end

	def posts
		@posts = Admin::Post.where({Published: true}).last(20).reverse
	end

	def post
		@post = Admin::Post.find(params[:id])
		rescue ActiveRecord::RecordNotFound
			raise ActionController::RoutingError.new "#{params[:id]} does not direct to a record"
	end
end
