class Admin::PostsController < AdminController
	before_action :set_post, only: [:show, :edit, :update, :destroy]
	# permissions checks
	before_action only: [:show, :new, :edit] { check_permission :EditPosts }
	before_action only: [:destroy] { check_permission :DeletePosts }

	# GET /admin/posts
	# GET /admin/posts.json
	def index
		@posts = Admin::Post.all.reverse_order
	end

	# GET /admin/posts/1
	# GET /admin/posts/1.json
	def show
	end

	# GET /admin/posts/new
	def new
		@post = Admin::Post.new
	end

	# GET /admin/posts/1/edit
	def edit
	end

	# POST /admin/posts
	# POST /admin/posts.json
	def create
		@post = Admin::Post.new(post_params)

		respond_to do |format|
			if @post.save
				format.html { redirect_to @post, notice: 'Post was successfully created.' }
				format.json { render :show, status: :created, location: @post }
			else
				format.html { render :new }
				format.json { render json: @post.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /admin/posts/1
	# PATCH/PUT /admin/posts/1.json
	def update
		respond_to do |format|
			if @post.update(post_params)
				format.html { redirect_to @post, notice: 'Post was successfully updated.' }
				format.json { render :show, status: :ok, location: @post }
			else
				format.html { render :edit }
				format.json { render json: @post.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /admin/posts/1
	# DELETE /admin/posts/1.json
	def destroy
		@post.destroy
		respond_to do |format|
			format.html { redirect_to admin_posts_url, notice: 'Post was successfully destroyed.' }
			format.json { head :no_content }
		end
	end

	private
	# Use callbacks to share common setup or constraints between actions.
	def set_post
		@post = Admin::Post.find(params[:id])
	end

	# Never trust parameters from the scary internet, only allow the white list through.
	def post_params
		params.require(:admin_post).permit(:Title, :Image, :Text, :Date, :user_id, :category_id, :Published)
	end
end
