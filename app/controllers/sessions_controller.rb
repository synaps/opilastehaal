class SessionsController < ApplicationController
  layout 'session_application'

  def new
  end

  def create
    @user = Admin::User.find_by(Nickname: params[:session]['Nickname'])
    if @user and @user.authenticate params[:session]['Password']
      log_in @user
      redirect_to root_path
    else
      flash.now['Error'] = 'Invalid login/password'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_path
  end
end
