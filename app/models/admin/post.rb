class Admin::Post < ActiveRecord::Base
	validates :Title, :Date, :user, :category, presence: true
	# relationships
	belongs_to :user
	belongs_to :category
	has_many :comments

	def to_s
		"#{self.id}"
	end
end
