class Admin::User < ActiveRecord::Base
	# validations
	before_validation 'align_privileges'
	validates :Name, :Surname, :Email, :Group, :privilege, presence: true
	validates :Email, uniqueness: true
	# settings
	has_secure_password     # secure passwords for our users

	enum Group: {user: 0, admin: 1, root: 2}
	# relationship
	belongs_to :privilege
	has_many :posts, dependent: :destroy
	has_many :comments

	def Group=(value)
		if value.kind_of?(String) and value.to_i.to_s == value
			super value.to_i
		else
			super value
		end
	end

	def to_s
		"#{self.Nickname}"
	end

	def check_privilege(privilege)
		self.privilege[privilege]
	end

	private
	def align_privileges
		unless self.privilege.nil?
			return
		end
		if self.user?
			self.privilege = Admin::Privilege.find(3)
		elsif self.admin?
			self.privilege = Admin::Privilege.find(2)
		elsif self.root?
			self.privilege = Admin::Privilege.find(1)
		end
	end
end
