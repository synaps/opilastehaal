class Admin::Comment < ActiveRecord::Base
	# validations
	validates :Text, length: {minimum: 60}      # set minimum length to 60 characters
	validates :user, :post, presence: true
	# relationships
	belongs_to :user
	belongs_to :post
end
