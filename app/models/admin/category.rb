class Admin::Category < ActiveRecord::Base
	# validations
	validates :Title, presence: true
	# relationship
	has_many :posts
	has_one :category
	belongs_to :category

	def to_s
		"#{self.Title}"
	end
end
