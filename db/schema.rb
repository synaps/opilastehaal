# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160321101906) do

  create_table "categories", force: :cascade do |t|
    t.string  "Title",       limit: 255,   null: false
    t.text    "Description", limit: 65535
    t.integer "category_id", limit: 4
  end

  create_table "comments", force: :cascade do |t|
    t.text    "Text",    limit: 65535
    t.date    "Date"
    t.boolean "Allowed"
    t.integer "user_id", limit: 4
    t.integer "post_id", limit: 4
  end

  add_index "comments", ["post_id"], name: "fk_rails_2fd19c0db7", using: :btree
  add_index "comments", ["user_id"], name: "fk_rails_03de2dc08c", using: :btree

  create_table "posts", force: :cascade do |t|
    t.string  "Title",       limit: 255,                   null: false
    t.string  "Image",       limit: 255
    t.text    "Text",        limit: 65535,                 null: false
    t.date    "Date",                                      null: false
    t.integer "user_id",     limit: 4,     default: 1
    t.integer "category_id", limit: 4,     default: 1
    t.boolean "Published",                 default: false
  end

  add_index "posts", ["category_id"], name: "fk_rails_9b1b26f040", using: :btree
  add_index "posts", ["user_id"], name: "fk_rails_5b5ddfd518", using: :btree

  create_table "privileges", force: :cascade do |t|
    t.boolean "CreateUsers",      default: false, null: false
    t.boolean "EditUsers",        default: false, null: false
    t.boolean "DeleteUsers",      default: false, null: false
    t.boolean "CreateCategories", default: false, null: false
    t.boolean "EditCategories",   default: false, null: false
    t.boolean "DeleteCategories", default: false, null: false
    t.boolean "CreatePosts",      default: false, null: false
    t.boolean "EditPosts",        default: false, null: false
    t.boolean "DeletePosts",      default: false, null: false
    t.boolean "CreateComments",   default: false, null: false
    t.boolean "EditComments",     default: false, null: false
    t.boolean "DeleteComments",   default: false, null: false
    t.boolean "EditPrivileges",   default: false, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string  "Nickname",        limit: 255,             null: false
    t.string  "Name",            limit: 255,             null: false
    t.string  "Surname",         limit: 255,             null: false
    t.integer "Group",           limit: 4,   default: 0, null: false
    t.integer "privilege_id",    limit: 4
    t.string  "Email",           limit: 255,             null: false
    t.string  "password_digest", limit: 255
  end

  add_index "users", ["privilege_id"], name: "fk_rails_06da574c0e", using: :btree

  add_foreign_key "comments", "posts"
  add_foreign_key "comments", "users"
  add_foreign_key "posts", "categories"
  add_foreign_key "posts", "users"
  add_foreign_key "users", "privileges"
end
