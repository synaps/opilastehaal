class CreateAdminUsers < ActiveRecord::Migration
	def change
		create_table :users do |t|
			t.string :Nickname, null: false
			t.string :Name, null: false
			t.string :Surname, null: false
			t.integer :Group, default: 0, null: false
			t.integer :privilege_id
			t.string :Email, null: false
			t.string :password_digest
		end

		add_foreign_key :users, :privileges, column: 'privilege_id', index: false
	end
end
