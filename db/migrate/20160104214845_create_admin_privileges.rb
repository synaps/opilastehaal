class CreateAdminPrivileges < ActiveRecord::Migration
  def change
    create_table :privileges do |t|
      t.boolean :ViewUsers, default: false, null: false
      t.boolean :CreateUsers, default: false, null: false
      t.boolean :EditUsers, default: false, null: false
      t.boolean :DeleteUsers, default: false, null: false
      t.boolean :ViewCategories, default: false, null: false
      t.boolean :CreateCategories, default: false, null: false
      t.boolean :EditCategories, default: false, null: false
      t.boolean :DeleteCategories, default: false, null: false
      t.boolean :ViewPosts, default: false, null: false
      t.boolean :CreatePosts, default: false, null: false
      t.boolean :EditPosts, default: false, null: false
      t.boolean :DeletePosts, default: false, null: false
      t.boolean :ViewComments, default: false, null: false
      t.boolean :CreateComments, default: false, null: false
      t.boolean :EditComments, default: false, null: false
      t.boolean :DeleteComments, default: false, null: false
      t.boolean :EditPrivileges, default: false, null: false
    end
  end
end
