class CreateAdminPosts < ActiveRecord::Migration
	def change
		create_table :posts do |t|
			t.string :Title, null: false
			t.string :Image
			t.text :Text, null: false
			t.date :Date, null: false
			t.integer :user_id, default: 1
			t.integer :category_id, default: 1
		end

		add_foreign_key :posts, :users, index: false
		add_foreign_key :posts, :categories, index: false
	end
end
