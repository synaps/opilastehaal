class CreateAdminComments < ActiveRecord::Migration
	def change
		create_table :comments do |t|
			t.text :Text
			t.date :Date
			t.boolean :Allowed
			t.integer :user_id
			t.integer :post_id
		end

		add_foreign_key :comments, :users, index: false
		add_foreign_key :comments, :posts, index: false
	end
end
