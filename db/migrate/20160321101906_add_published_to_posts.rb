class AddPublishedToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :Published, :boolean, default: false
  end
end
