class CreateAdminCategories < ActiveRecord::Migration
	def change
		create_table :categories do |t|
			t.string :Title, null: false
			t.text :Description
		end

		add_reference 'categories', 'category'
	end
end
