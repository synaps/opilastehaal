# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

@root_privilege = Admin::Privilege.create(id: 1, ViewUsers: true, CreateUsers: true, EditUsers: true, DeleteUsers: true, ViewCategories: true, CreateCategories: true, EditCategories: true, DeleteCategories: true, ViewPosts: true, CreatePosts: true, EditPosts: true, DeletePosts: true, ViewComments: true, CreateComments: true, EditComments: true, DeleteComments: true, EditPrivileges: true)
@admin_privilege = Admin::Privilege.create(id: 2, ViewUsers: true, CreateUsers: true, ViewCategories: true, CreateCategories: true, EditCategories: true, DeleteCategories: true, ViewPosts: true, CreatePosts: true, EditPosts: true, DeletePosts: true, ViewComments: true, CreateComments: true, EditComments: true, DeleteComments: true,)
@user_privilege = Admin::Privilege.create(id: 3)
@user = Admin::User.create!(id: 1, Nickname: 'admin', Name: 'admin', Surname: 'admin', Group: 2, password: 'StrongPassword', password_confirmation: 'StrongPassword', Email: 'rahimgulov.rustam@gmail.com', privilege: @root_privilege)
@category = Admin::Category.create! id: 1, Title: 'default'
@post = @user.posts.create Title: 'Post1', Text: 'Post contents here', Date: '2016-11-14'
@comment = @user.comments.create Text: 'Indexes are special lookup tables that the database search engine can use to speed up data retrieval.', post: @post
