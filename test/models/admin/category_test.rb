require 'test_helper'

class Admin::CategoryTest < ActiveSupport::TestCase
	def test_create_empty
		assert_not Admin::Category.new.save, 'Empty \'Category\' saved'
	end

	def test_create_normal
		@category = Admin::Category.new({Title: 'Title1'})
		assert @category.save
	end

	def test_create_sub
		@category = Admin::Category.new({Title: 'Title1'})
		@category.save
		@sub = Admin::Category.new({Title: 'sub1', category: @category})
		assert @sub.save, 'Failed to save \'Subcategory\'.'
		assert_not_nil @sub.category, 'Parent \'Category\' is nil'
	end
end
