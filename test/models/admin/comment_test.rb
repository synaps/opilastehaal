require 'test_helper'

class Admin::CommentTest < ActiveSupport::TestCase
	def test_create_empty
		assert_not Admin::Comment.new.save
	end

	def test_create_normal
		@post = Admin::Post.new({Title: 'Title1', Text: 'some text', Date: '2016-01-04'})
		@post.save
		@comment = Admin::User.first.comments.new({Text: 'simple comment containing at least sixty characters to satisfy verification', post: @post})
		assert @comment.save
	end

	def test_create_wrong
		@comment = Admin::User.first.comments.new({Text: 'simple comment.'})
		assert_not @comment.save, 'Wrong \'Comment\' saved'
	end
end
