require 'test_helper'

class Admin::CommentsControllerTest < ActionController::TestCase
  setup do
    @admin_comment = admin_comments(:one)
  end

  test 'get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_comments)
  end

  test 'get new' do
    get :new
    assert_response :success
  end

  test 'show admin_comment' do
    get :show, id: @admin_comment
    assert_response :success
  end

  test 'get edit' do
    get :edit, id: @admin_comment
    assert_response :success
  end

end
