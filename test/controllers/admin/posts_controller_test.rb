require 'test_helper'

class Admin::PostsControllerTest < ActionController::TestCase
  setup do
    @admin_post = admin_posts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_posts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should show admin_post" do
    get :show, id: @admin_post
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_post
    assert_response :success
  end
end
