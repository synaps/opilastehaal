require 'test_helper'

class Admin::CategoriesControllerTest < ActionController::TestCase
  setup do
    @admin_category = admin_categories(:one)
  end

  test 'get index' do
    get :index
    assert_response :success
  end

  test 'get new' do
    get :new
    assert_response :success
  end

  test 'show admin_category' do
    get :show, id: @admin_category
    assert_response :success
  end

  test 'get edit' do
    get :edit, id: @admin_category
    assert_response :success
  end
end
