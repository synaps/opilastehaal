require 'test_helper'

class Admin::UsersControllerTest < ActionController::TestCase
  setup do
    @admin_user = admin_users(:one)

    @admin_user_hash = @admin_user.attributes
    @admin_user_hash.update({'Group' => 'admin'})
  end

  test 'get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_users)
  end

  test 'show admin_user' do
    get :show, id: @admin_user
    assert_response :success
  end

  test 'get new' do
    get :new
    assert_response :success
  end

  test 'create admin_user' do
    assert_difference 'Admin::User.count' do
      post :create, admin_user: @admin_user_hash
    end
  end

  test 'get edit' do
    get :edit, id: @admin_user
    assert_response :success
  end
end
