ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
end


# assert_select(selector, [equality], [message]) ensures that the equality
# condition is met on the selected elements through the selector. The selector
# may be a CSS selector expression (String) or an expression with substitution
# values.

# assert_select(element, selector, [equality], [message]) ensures that the
# equality condition is met on all the selected elements through the selector
# starting from the element (instance of Nokogiri::XML::Node or
# Nokogiri::XML::NodeSet) and its descendants.